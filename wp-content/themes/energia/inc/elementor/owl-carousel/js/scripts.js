jQuery(function(){
    if (typeof owlCarousel === "undefined") {
        var scriptOwl = document.createElement("script");
        scriptOwl.src =
            "https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"; // URL do seu script aqui
        document.body.appendChild(scriptOwl);
    } 


    jQuery(".owl-carousel").owlCarousel({
        items: 3,
        loop: true,
        nav: true,
        autoplay: true,
        margin: 20,
        dots: false,
        responsive: {
            0: {
                items: 1,
                margin: 20
            },
            767: {
                items: 3,
                margin: 20,
            }
        }
    });
})
