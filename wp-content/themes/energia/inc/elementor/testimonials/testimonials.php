<?php 

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

final class OMElementorTestimonials {


    private static $_instance = null;

    public static function instance() {

        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }

         return self::$_instance;
    }

    public function __construct() {	
        add_action( 'init', [ $this, 'init' ] ); 
    }

    public function init() {
        add_action( 'elementor/widgets/register', [ $this, 'init_widgets' ] );
    }

    public function init_widgets($widgets_manager) {

        require_once( __DIR__ . '/widget/widget.php' );
        // \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \CSPElementorBannerWidget() );
        $widgets_manager->register( new \OMElementorTestimonialsWidget() );

    }

}

OMElementorTestimonials::instance();


?>