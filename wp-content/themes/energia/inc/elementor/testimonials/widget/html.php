<div class="calendario-vacinal"  ajax="<?php echo $settings['ajax'] ?>"  <?php echo isset($_GET['faixa-etaria']) ?  'id="anchorCalendarioVacinal"' : ''?>>
    <div class="container">
        <div class="col-12 quem-vai-box">
            <div class="box-quem-vai">
                <h1>
                <?php 
                    echo empty($settings['title']) ? 'QUEM VAI TOMAR A VACINA?' : $settings['title'];
                ?>    
                </h1>
                <p>
                    <?php 
                    echo empty($settings['description']) ? 'Escolha a idade abaixo e conheça todo ciclo vacinal disponível no Sabin!' : $settings['description'];
                    ?>
                    
                </p>
                <div class="box-categoria row">
                    <?php 
                    $terms = get_terms( array(
                        'taxonomy' => 'faixa-etaria',
                        'hide_empty' => false,
                    ) );
                    foreach($terms as $term) {
                        $icon = function_exists('get_field') ? get_field('icone', $term->taxonomy . '_' . $term->term_id) : '';
                        $active = (isset($_GET['faixa-etaria'])) ? ($_GET['faixa-etaria'] == $term->slug ? 'active' : '') : '';
                        $link = $term->slug == 'bebes' ? 'javascript:return false;' : get_permalink($settings['page']).'?faixa-etaria='.$term->slug;
                        $onClick = $term->slug == 'bebes' ? 'onClick="document.querySelector(\'.baby-timeline\').style.display=\'block\'"' : '';
                    ?>
                    <div class="item col-6 col-lg-2">
                        <a href="<?php echo $link ?>" <?php echo $onClick ?> rel="<?php echo $term->slug ?>" class="<?php echo $term->slug ?> <?php echo $active ?>">
                            <?php if(!empty($icon)) { ?>
                                <img src="<?php echo $icon ?>" alt="">
                            <?php } ?>
                            <?php echo $term->name ?>
                        </a>
                    </div>
                    <?php 
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="filter-section">

        <div class="container baby-timeline" <?php echo (isset($_GET['faixa-etaria']) AND $_GET['faixa-etaria'] == 'bebes') ? 'style="display:block;"' : ''; ?>>
            <div class="row">
                <div class="col-lg-2 texto">
                    <p>
                        Vacina para o bebê de quantos meses?
                    </p>
                </div>
                <div class="col-12 mobile-show">
                    <div class="owl-carousel owl-carousel-months-vacinnes owl-theme timeline">
                        <?php 
                        $months = 12;
                        for($i=1;$i<=$months;$i++) {
                            $link = get_permalink($settings['page'])."?faixa-etaria=bebes&meses={$i}";
                            $active = isset($_GET['meses']) ? (($_GET['meses'] == $i) ? 'active-carrossel' : '') : '';
                            ?>
                            <div class="item">
                                <a href="<?php echo $link ?>" month="<?php echo $i; ?>" class="<?php echo isset($_GET['meses']) ? (($_GET['meses'] == $i) ? 'active' : '') : '' ?>">
                                    <h1 class="circle <?php echo $active ?>"><?php echo $i; ?></h1>
                                </a>
                            </div>
                        <?php } ?>               
                    </div>
                    <div class="gray-bar">
                    </div>
                </div>
                <div class="col-lg-10 desktop-show">
                    <div class="timeline">
                        <?php 
                        $months = 12;
                        for($i=1;$i<=$months;$i++) {
                            $link = get_permalink($settings['page'])."?faixa-etaria=bebes&meses={$i}";
                            $active = isset($_GET['meses']) ? (($_GET['meses'] == $i) ? 'active-carrossel' : '') : '';
                            ?>
                            <div class="item">
                                <a href="<?php echo $link ?>" month="<?php echo $i; ?>" class="<?php echo isset($_GET['meses']) ? (($_GET['meses'] == $i) ? 'active' : '') : '' ?>">
                                    <h1 class="circle <?php echo $active ?>"><?php echo $i; ?></h1>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="gray-bar">
                    </div>
                </div>

            </div>
        </div>
    

        <div class="container">
            <div class="row">
                <div class="col-12 vacinas-bloco">
                    <?php 
                    // $posts = array(
                    //     'post_type' => 'vacinas',
                    //     'posts_per_page' => '-1',
                    //	   'orderby' => 'menu_order',
                    //     'order' => 'ASC'
                    // ); 

                    // if(isset($_GET['faixa-etaria'])) {
                    //     $posts['tax_query'] = array(
                    //         array(
                    //             'taxonomy' => 'faixa-etaria',
                    //             'field'    => 'slug',
                    //             'terms'    => $_GET['faixa-etaria'],
                    //         )
                    //     );
                    // }

                    // if(isset($_GET['meses'])) {
                    //     // $posts['meta_key'] = 'meses';
                    //     $posts['meta_query'] = array(
                    //         array(
                    //             'key'     => 'meses',
                    //             'value'   => serialize(strval($_GET['meses'])),
                    //             'compare' => 'LIKE',
                    //         ),
                    //         array(
                    //             'key'     => 'regional',
                    //             'value'   => serialize(strval($regional)),
                    //             'compare' => 'LIKE',
                    //         ),
                    //     );
                    // } else {
                    //     $posts['meta_query'] = array(
                    //         array(
                    //             'key'     => 'regional',
                    //             'value'   => serialize(strval($regional)),
                    //             'compare' => 'LIKE',
                    //         ),
                    //     );
                    // }

                    // if(isset($_GET['faixa-etaria'])) {
                    //     if($_GET['faixa-etaria'] == 'bebes' AND !isset($_GET['meses'])) {
                    //         $posts = array();
                    //     } else {
                    //         $posts = get_posts($posts);
                    //     }
                    // } else {
                    //     $posts = array();
                    // }

                    $posts = [];
                    foreach($posts as $vaccine) {
                        $valor = get_post_meta($vaccine->ID, 'valor', true);
                        $blank = get_post_meta($vaccine->ID, 'blank', true);
                        $target = ($blank) ? 'target="_blank"' : '';

                        $price = (empty($valor)) ? '' : '
                            <h3>
                                <span class="price">
                                    R$
                                </span>
                                '.get_post_meta($vaccine->ID, 'valor', true).'
                                <span class="parcela">
                                    ou 
                                    '.get_post_meta($vaccine->ID, 'valor_parcelado', true).'
                                </span>
                            </h3>
                        ';
                    ?>
                    <div class="bloco-vacina">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 title-vacina">
                                <h2><?php echo $vaccine->post_title ?></h2>
                                <div class="redline"></div>
                                <!-- <?php print_r(get_post_meta($vaccine->ID, 'regional', true)) ?> -->
                                <p><?php echo apply_filters('the_content', $vaccine->post_content) ?>
                                </p>
                            </div>
                            <div class="col-lg-4 col-md-6 doses-vacina">
                                <h2>Esquema de doses</h2>
                                <div class="redline"></div>
                                <p>
                                    <?php 
                                    echo (function_exists('the_field')) ? get_field('esquema_de_doses', $vaccine->ID) : '';
                                    ?>
                                </p>
                            </div>
                            <div class="col-lg-4 col-md-12 card-vacina">
                                <div class="card-loja">
                                    <div class="row">
                                        <div class="col-4">
                                            <img src="<?php echo get_the_post_thumbnail_url($vaccine->ID) ?>" alt="">
                                        </div>
                                        <div class="col-8 text-card">
                                            <h2>
                                            <?php 
                                                echo (function_exists('the_field')) ? get_field('nome_na_loja', $vaccine->ID) : '';
                                            ?>
                                            </h2>
                                            <?php echo $price ?>
                                            <?php 
                                            $link = (function_exists('the_field')) ? get_field('link', $vaccine->ID) : '#';
                                            ?>
                                            <a href="<?php echo $link ?>" <?php echo $target ?>>COMPRAR VACINA</a>
                                        </div>
                                    </div>

                                </div>
                                <p>
                                <?php 
                                    echo (function_exists('the_field')) ? get_field('observacao', $vaccine->ID) : '';
                                ?>
                                </p>
                            </div>
                        </div>
                        <div class="separator"></div>
                    </div>
                    <?php } ?>
                    
                </div>
            </div>
        </div>

    </div>
</div>