<?php 
class OMElementorTestimonialsWidget extends \Elementor\Widget_Base {

	public function __construct($data = [], $args = null)
	{
		parent::__construct($data, $args);
		wp_register_style( 'swiper-slide', 'https://unpkg.com/swiper@8/swiper-bundle.min.css', [], '1.0.0' );
		wp_register_script( 'swiper-slide', 'https://unpkg.com/swiper@8/swiper-bundle.min.js', [ 'elementor-frontend', 'jquery' ], '1.0.0', true );
	}

	public function get_style_depends() {
		return [ 'swiper-slide' ];
	}

	public function get_script_depends() {
		return [ 'swiper-slide' ];
	}

	public function get_name() {
        return 'OMElementorTestimonialsWidget';
    }

	public function get_title() {
        return 'Depoimentos';
    }

	public function get_icon() {
        return 'eicon-testimonial-carousel';
    }

	protected function register_controls() {


    }

	protected function render() {

        $posts = get_posts([
			'post_type' => 'depoimentos',
			'posts_per_page' => '-1'
		]);
	?>
		<div class="OMElementorTestimonialsWidget-container">
			<div class="testimonials">
				<div class="swiper mySwiper">
					<div class="swiper-wrapper">
						<?php 
						foreach($posts as $post) {
						?>
							<div class="swiper-slide">
								<div class="image">
									<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" alt="<?php echo $post->post_title ?>">
								</div>
								<div class="content">
									<p class="testimonial"><?php echo $post->post_content ?></p>
									<p class="author"><?php echo $post->post_title ?></p>
								</div>
							</div>
						<?php } ?>
					</div>
					
				</div>
			</div>
			<div class="swiper-pagination"></div>
		</div>

		<script>
			jQuery(function(){
				var swiper = new Swiper(".mySwiper", {
					pagination: {
					el: ".swiper-pagination",
					clickable: true,
					renderBullet: function (index, className) {
						return '<span class="' + className + '"></span>';
					},
					},
				});
			})
		
		</script>
	<?php
    }

}

?>