<?php 
add_action( 'customize_register', function($wp_customize){

    //   HEADER LOGO
      $wp_customize->add_setting( 'header_logo');
      $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_logo', array(
        'label' => 'Logo',
        'priority' => 20,
        'section' => 'title_tagline',
        'settings' => 'header_logo',
        'description' => 'Defina a logo do seu site, a mesma será exibida no cabeçalho',
        'button_labels' => array(// All These labels are optional
                    'select' => 'Selecionar Logo',
                    'remove' => 'Remover Logo',
                    'change' => 'Definir Logo',
                    )
      )));

    //   HEADER FOOTER
      $wp_customize->add_setting( 'footer_logo');
      $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_logo', array(
        'label' => 'Logo Rodapé',
        'priority' => 20,
        'section' => 'title_tagline',
        'settings' => 'footer_logo',
        'description' => 'Defina a logo do rodapé seu site',
        'button_labels' => array(// All These labels are optional
                    'select' => 'Selecionar Logo',
                    'remove' => 'Remover Logo',
                    'change' => 'Definir Logo',
                    )
      )));


      // TELEFONE
      $wp_customize->add_setting( 'telephone');
      $wp_customize->add_control('telephone', array(
        'label' => __('Telefone', 'mrreformas'),
        'priority' => 20,
        'section' => 'title_tagline',
        'settings' => 'telephone',
        'description' => 'Informe o telefone de sua empresa.',
      ));


      // LOCATION
      $wp_customize->add_setting( 'location');
      $wp_customize->add_control('location', array(
        'type' => 'url',
        'label' => __('Endereço', 'mrreformas'),
        'priority' => 20,
        'section' => 'title_tagline',
        'settings' => 'location',
        'description' => 'Informe o endereço de sua empresa.',
      ));

      // SOCIAL INSTAGRAM
      $wp_customize->add_setting( 'social_instagram');
      $wp_customize->add_control('social_instagram', array(
        'type' => 'url',
        'label' => __('Instagram', 'mrreformas'),
        'priority' => 20,
        'section' => 'title_tagline',
        'settings' => 'social_instagram',
        'description' => 'Informe a url do instagram de sua empresa.',
      ));

      // SOCIAL INSTAGRAM
      $wp_customize->add_setting( 'social_whatsapp');
      $wp_customize->add_control('social_whatsapp', array(
        'label' => __('Whastsapp', 'mrreformas'),
        'priority' => 20,
        'section' => 'title_tagline',
        'settings' => 'social_whatsapp',
        'description' => 'Informe o número de whatsapp da sua empresa no seguinte formato código do pais+ddd+número. Exemplo: 5548984627444',
      ));

} );

?>