<?php 
get_header();
?>

<section class="main-content">
    <?php 
    if(have_posts()) { 
        the_post();
    ?>
        <h2 class="page-title"><?php the_title() ?></h2>
        <div class="page-content"><?php the_content(); ?></div>
    <?php
    } else {
        echo __('Página não encontrada', 'mrreformas');
    }
    ?>
</section>

<?php 
get_footer();
?>