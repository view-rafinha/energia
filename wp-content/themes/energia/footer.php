<footer>
    <div class="footer-top container">
        <div class="logo">
            <?php 
                $header_logo = get_theme_mod('footer_logo');
                if(!empty($header_logo)) {
            ?>
                <a href="<?php echo site_url(); ?>">
                    <img src="<?php echo $header_logo ?>" alt="<?php echo get_bloginfo() ?>">
                </a>
            <?php 
                }
            ?>
            
        </div>
        <div class="menu">
            <?php 
            if ( has_nav_menu( 'footer_menu' ) ) {
                wp_nav_menu(
                    array(
                        'container'  => '',
                        'items_wrap' => '%3$s',
                        'theme_location' => 'footer_menu',
                    )
                );
            }
            ?>
        </div>
        
    </div>
    <hr class="container">
    <div class="footer-bottom container">
        <div class="left">
            <h2>Localização</h2>
            <?php 
            $location = get_theme_mod('location');
            echo $location;
            ?>
        </div>
        <div class="right">
            <?php 
            $instagram = get_theme_mod('footer_social_instagram');
            $whatsapp = get_theme_mod('footer_social_whatsapp');
            ?>
            <h2>Redes Sociais</h2>
            <a href="<?php echo $instagram ?>" target="_blank" class="instagram">
                <img src="<?php echo get_stylesheet_directory_uri().'/assets/img/ico-whatsapp.png' ?>" alt="Whatsapp">
            </a>
            <a href="https://wa.me/<?php echo $whatsapp ?>" target="_blank" class="whatsapp">
                <img src="<?php echo get_stylesheet_directory_uri().'/assets/img/ico-instagram.png' ?>" alt="Whatsapp">
            </a>
        </div>
    </div>
    
</footer>

<?php wp_footer(); ?>

</body>